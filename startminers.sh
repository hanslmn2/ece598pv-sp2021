#!/bin/bash

lambda=2000000
port_start=7000
port_delta=10
num_miners=3
num_workers=8

i=0
while [ $i -lt $num_miners ]; do
    prev_i=$(($i-1))
    p2p_port=$((6000+$port_delta*$i))
    api_port=$((7000+$port_delta*$i))
    prev_port=$((6000+$port_delta*$prev_i))

    echo "Starting miner $i..."

    if [ $i -gt 0 ]; then
        cargo run -- -vvv --p2p-workers $num_workers --p2p 127.0.0.1:$p2p_port --api 127.0.0.1:$api_port -c 127.0.0.1:$prev_port > miner_log_$i.log 2>&1 &
    else
        cargo run -- -vvv --p2p-workers $num_workers --p2p 127.0.0.1:$p2p_port --api 127.0.0.1:$api_port > miner_log_$i.log 2>&1 &
    fi

    let i=i+1
done

read -p "Press any key when all miners have initialized and connected..."

i=0
while [ $i -lt $num_miners ]; do
    api_port=$((7000+$port_delta*$i))
    curl localhost:$api_port/miner/start?lambda=$lambda
    let i=i+1
done

read -p "Press enter to stop miners"
pkill bitcoin
use super::message::Message;
use super::peer;
use crate::network::server::Handle as ServerHandle;
use crate::blockchain::Blockchain;
use crate::block::Block;
use crossbeam::channel;
use log::{debug, warn, info};
use crate::crypto::hash::{H256, Hashable, H160};

use crate::transaction::{SignedTransaction, UTXO_output, UTXO_input, Transaction, verify};
use crate::mempool::{State,Mempool,AddressBook};

use std::thread;
use std::sync::{Arc, Mutex};
use std::collections::HashMap;
use std::time::{SystemTime, UNIX_EPOCH, Duration};

use ring::digest;
use ring::signature::{self, Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};

use rand::Rng;


#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    orphan_buffer: Arc<Mutex<HashMap<H256, Block>>>,
    mempool: Arc<Mutex<Mempool>>,
    state: Arc<Mutex<State>>,
    addressbook: Arc<Mutex<AddressBook>>,
    count: usize,
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    orphan_buffer: &Arc<Mutex<HashMap<H256, Block>>>,
    mempool: &Arc<Mutex<Mempool>>,
    state: &Arc<Mutex<State>>,
    addressbook: &Arc<Mutex<AddressBook>>,

) -> Context {
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        orphan_buffer: Arc::clone(orphan_buffer),
        mempool: Arc::clone(mempool),
        state: Arc::clone(state),
        addressbook: Arc::clone(addressbook),
        count: 0
    }
}
// let addressbook_lock = addressbook.lock().unwrap();
// server.broadcast(Message::NewAddress(addressbook_lock.get_my_address().clone()));
// drop(addressbook_lock);
impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });

        }

    }

    fn worker_loop(&self) {
        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();
            match msg {
                Message::Ping(nonce) => {
                    // debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    // debug!("Pong: {}", nonce);
                }
                Message::NewBlockHashes(hashes) => {
                    // debug!("Incoming: NewBlockHashes({:?})", hashes);

                    // Create empty vector to store hashes of blocks that we don't have a record of
                    let mut missing_hashes: Vec<H256> = Vec::new();

                    // Check if there is a record of the hash, and record for later if there is no record
                    for hash in hashes {
                        if !self.blockchain.lock().unwrap().hash_to_block.contains_key(&hash) {
                            missing_hashes.push(hash);
                        }
                    }

                    if !missing_hashes.is_empty() {
                        // Request blocks of new hashes from sender
                        // debug!("To Peer: GetBlocks({:?})", missing_hashes);
                        peer.write(Message::GetBlocks(missing_hashes.clone()));
                    }
                }
                Message::GetBlocks(hashes) => {
                    // debug!("Incoming: GetBlocks({:?})", hashes);

                    // Create empty vector to store hashes of blocks that we do have a record of
                    let mut requested_blocks: Vec<Block> = Vec::new();

                    for hash in hashes {
                        match self.blockchain.lock().unwrap().hash_to_block.get(&hash) {
                            Some(block) => requested_blocks.push(block.clone()),
                            None => {}
                        }
                    }

                    if !requested_blocks.is_empty() {
                        // debug!("To Peer: Blocks(...)");
                        peer.write(Message::Blocks(requested_blocks));
                    }

                }
                Message::Blocks(blocks) => {
                    // debug!("Incoming: Blocks({:?})", blocks);

                    let mut blockchain_exclusive = self.blockchain.lock().unwrap();

                    // Create empty vector to store hashes of new blocks
                    let mut new_hashes: Vec<H256> = Vec::new();
                    
                    for block in blocks {
                        // Calculate block delay                       
                        // let block_delay = SystemTime::now().duration_since(UNIX_EPOCH).unwrap() - block.header.timestamp;
                        // info!("INFO: received block with a delay since discovery of {:?}.", block_delay);                

                        if !blockchain_exclusive.hash_to_block.contains_key(&block.hash()) {
                            
                            let transactions = &block.content.transactions;
                            let mut buffer = self.orphan_buffer.lock().unwrap();
                            let block_hash: H256 = block.hash();
                            
                            
                            if blockchain_exclusive.hash_to_block.contains_key(&block.header.parent_hash) {
                                let mut state_mutex = self.state.lock().unwrap();
                                let mut mempool_mutex = self.mempool.lock().unwrap();

                                // Proof of work check
                                let pow_good = block_hash <= block.header.difficulty && 
                                    block.header.difficulty == blockchain_exclusive.hash_to_block[&block.header.parent_hash].header.difficulty;
                                
                                // Transaction properties check
                                let state_at_block_parent = &blockchain_exclusive.hash_to_state[&block.header.parent_hash];
                                let mut tx_good = true;
                                for tx in transactions {
                                    tx_good = tx_good && transaction_check(&tx, &state_at_block_parent);
                                }
                                if !tx_good {
                                    debug!("New Block: Transaction check failed!");
                                }

                                if pow_good && tx_good {
                                    blockchain_exclusive.insert(&block, &mut mempool_mutex, &mut state_mutex);
                                    new_hashes.push(block_hash);
                                    
                                    let mut temp_hash = block_hash;
                                    loop { //keep trying to add orphan blocks
                                        if buffer.contains_key(&temp_hash) { //there is a child in the orphan buffer                            
                                            let orphan_block = buffer.remove(&temp_hash).unwrap();
                                            let orphan_txs = & orphan_block.content.transactions;

                                            // Check PoW and TX Valid - TODO: could refactor into function for reuse
                                            let pow_good = orphan_block.hash() <= orphan_block.header.difficulty && 
                                                orphan_block.header.difficulty == blockchain_exclusive.hash_to_block[&orphan_block.header.parent_hash].header.difficulty;
                                           
                                            let orphan_state = &blockchain_exclusive.hash_to_state[&temp_hash];
                                            let mut tx_good = true;
                                            for tx in orphan_txs {
                                                tx_good = tx_good && transaction_check(&tx, &orphan_state);
                                            }
                                            if !tx_good {
                                                debug!("Orpah Block: Transaction check failed!");
                                            }
                                            if pow_good && tx_good{
                                                blockchain_exclusive.insert(&block, &mut mempool_mutex, &mut state_mutex);
                                                new_hashes.push(orphan_block.hash());
                                            }

                                            // Continue connecting orphans with parents
                                            temp_hash = orphan_block.header.parent_hash;
                                        }
                                        else {
                                            break;
                                        }
                                    }
                                }
                            } else  {
                                debug!("Parent ({:?}) not found for incoming block ({:?})", &block.header.parent_hash, &block.hash());
                                let mut missing_parent_hash: Vec<H256> = Vec::new();
                                missing_parent_hash.push(block.header.parent_hash.clone());
                                buffer.insert(block.header.parent_hash, block);
                                peer.write(Message::GetBlocks(missing_parent_hash.clone())); 
                            }
                        }
                    }

                    if !new_hashes.is_empty() {
                        // Gossip to peers about new hashes
                        // debug!("Broadcast: NewBlockHashes({:?})", new_hashes);
                        self.server.broadcast(Message::NewBlockHashes(new_hashes.clone()));
                    }
                }
                Message::NewTransactionHashes(transaction_hashes) => {
                    // debug!("Incoming: NewTransactionHashes({:?})", transaction_hashes);

                    let mut mempool_mutex = self.mempool.lock().unwrap();
                    let mut tx_hashes = Vec::new();

                    for hash in transaction_hashes {
                        if !mempool_mutex.contains_hash(&hash) {
                            tx_hashes.push(hash);
                        }
                    }
                    if tx_hashes.len() > 0 {          
                        peer.write(Message::GetTransactions(tx_hashes));     
                    }            
                }
                Message::GetTransactions(transaction_hashes) => {
                    // debug!("Incoming: GetTransactions({:?})", transaction_hashes);

                    let mut mempool_mutex = self.mempool.lock().unwrap();
                    let mut transactions = Vec::new();

                    for hash in transaction_hashes {
                        if mempool_mutex.contains_hash(&hash) {
                            transactions.push(mempool_mutex.get(&hash).clone());
                        }
                        else{
                            // debug!("Transaction not found in mempool! ({:?})", hash);    
                        }
                    }
                    if transactions.len() > 0 {
                        peer.write(Message::Transactions(transactions));    
                    }
                }
                Message::Transactions(transactions) => {
                    // debug!("Incoming: Transactions ");
                    let mut mempool_mutex = self.mempool.lock().unwrap();
                    let mut state_mutex = self.state.lock().unwrap();
                    let mut tx_hashes = Vec::<H256>::new();

                    for tx in transactions {
                        if transaction_check(&tx, &state_mutex){

                            // Check that transaction does not reuse any UTXOs already in mempool
                            if !mempool_mutex.contains_double_utxo(&tx) {
                                let tx_hash: H256 = tx.hash();
                                // debug!("Mempool Inserting new transaction hash:({:?})", tx_hash);
                                tx_hashes.push(tx_hash);

                                mempool_mutex.insert(&tx);
                            }
                        }
                        else{
                            // debug!("Transaction check failed!");
                        }

                    }
                    if tx_hashes.len() > 0 {
                        self.server.broadcast(Message::NewTransactionHashes(tx_hashes)); 
                    }
                }
                Message::NewAddress(address) => {
                    // debug!("Incoming: new address: ({:?})", address);
                    let mut addressbook_mutex = self.addressbook.lock().unwrap();
                    addressbook_mutex.insert_addr(address.clone());
                    // debug!("Addressbook contents:");
                    for addr in addressbook_mutex.addresses.iter(){
                        // debug!("({:?})", addr);
                    }
                }

            }// match msg;
        }
    }
}

pub fn transaction_check(tx: &SignedTransaction, utxo: &State)->bool{
    let mut valid_txs:bool = true;

    //validate signature
    if !verify(
        &tx.transaction, 
        &tx.signature, 
        &tx.public_key){
            debug!("transaction check: invalid signature sign! ({:?})", tx.hash());
            valid_txs = false;
        }

    //consistent owner
    let mut tx_input_amount_count = 0;
    for tx_input in &tx.transaction.input {

        // Check if input exists in current state
        // Also takes care of double spend problem
        if utxo.state.contains_key(&(tx_input.prev_tx_hash, tx_input.index)) {
            let prev_tx_output = utxo.state[&(tx_input.prev_tx_hash, tx_input.index)];
            let input_amount = prev_tx_output.0;
            let prev_recipient_addr = prev_tx_output.1;
            
            tx_input_amount_count += input_amount;

            let public_key_hash: H256 = digest::digest(&digest::SHA256, &tx.public_key).into();

            // Check if input UTXO is owned by signer
            if prev_recipient_addr != public_key_hash.to_h160().into() {
                debug!("transaction check: UTXO included in transaction not owned by signer");
                valid_txs = false;
            }
        }
        else {
            debug!("transaction check: no unspend tx found ({:?} , ({:?}))", &(tx_input.prev_tx_hash), tx_input.index);
            valid_txs = false;
        }
    }

    // Over spend check
    // ICO has no source, can be overspent
    if !tx.transaction.is_ico {
        let mut tx_output_amount_count = 0;
        for txout in &tx.transaction.output{
            tx_output_amount_count += txout.value;
        }
        if tx_output_amount_count > tx_input_amount_count {
            debug!("transaction check: invalid tx amount! ({:?})", tx.hash());
            valid_txs = false;
        }
    }

    valid_txs
}

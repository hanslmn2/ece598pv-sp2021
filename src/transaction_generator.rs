
use log::{debug, warn, info};
use crate::crypto::hash::{H256, Hashable, H160};

use crate::transaction::{SignedTransaction, UTXO_output, UTXO_input, Transaction, verify};
use crate::mempool::{State,Mempool,AddressBook};
use crate::Message;
use crate::network::server::Handle as ServerHandle;

use std::thread;
use std::sync::{Arc, Mutex};
use std::collections::HashMap;
use std::time;
use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};

use ring::digest;
use ring::signature::{self, Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};

use rand::Rng;
use rand::seq::SliceRandom;

#[derive(Clone)]
pub struct Context {
    // control_chan: Receiver<ControlSignal>,
    server: ServerHandle,
    mempool: Arc<Mutex<Mempool>>,
    state: Arc<Mutex<State>>,
    addressbook: Arc<Mutex<AddressBook>>,
}

pub fn new(
    server: &ServerHandle,
    mempool: &Arc<Mutex<Mempool>>,
    state: &Arc<Mutex<State>>,
    addressbook: &Arc<Mutex<AddressBook>>,

) -> Context {

    let ctx = Context {
        // control_chan: signal_chan_receiver,
        server: server.clone(),
        mempool: Arc::clone(mempool),
        state: Arc::clone(state),
        addressbook: Arc::clone(addressbook),
    };
    
    ctx
}

impl Context {
    pub fn start(self) {
        thread::spawn(move || {
            self.generator_loop();
        });
    }

    fn generator_loop(&self) {
        let mut count = 0;

        // On startup, insert and ICO for yourself into the mempool
        {
            info!("Executing ICO - Generating Sourceless TX");
            
            let value: u32 = 9999;

            let addressbook_locked = self.addressbook.lock().unwrap();

            let ico_transaction = Transaction {
                input: Vec::new(),
                output: vec![
                    UTXO_output {
                        receipient_address: addressbook_locked.get_my_address().clone(),
                        value: value,
                    },
                ],
                is_ico: true,
            };

            let tx_serial = bincode::serialize(&ico_transaction).unwrap();
            let signature = addressbook_locked.my_identity.sign(&tx_serial);

            let signed_tx = SignedTransaction {
                transaction: ico_transaction,
                public_key: addressbook_locked.my_identity.public_key().as_ref().to_vec(), 
                signature: signature.as_ref().to_vec() 
            };

            //sanity check
            // let sc = verify(
            //     &signed_tx.transaction, 
            //     &signed_tx.signature,
            //     &signed_tx.public_key);
            // debug!("Sanity Check({:?})", sc); 

            let mut mempool_locked = self.mempool.lock().unwrap();
            mempool_locked.insert(&signed_tx);

            info!("ICO TX Generated. {:?} coin TX to {:?} inserted into mempool.",
                value, addressbook_locked.get_my_address());

            // Share ICO TX with others - they likely won't mine it, but should have it on hand
            let signed_tx_hash: H256 = signed_tx.hash();
            self.server.broadcast(Message::NewTransactionHashes(vec![signed_tx_hash]));
        }

        loop {
            count+=1;

            // Occasionally notify peers of your address - heartbeat
            if count % 50 == 0 {
                let addressbook_lock = self.addressbook.lock().unwrap();
                self.server.broadcast(Message::NewAddress(addressbook_lock.get_my_address().clone()));
                for addr in addressbook_lock.addresses.iter(){
                    self.server.broadcast(Message::NewAddress(addr.clone()));
                }
                drop(addressbook_lock);
            }

            // Start generating TXs if you have funds to do so
            if count % 10 == 0 {

                let state_locked = self.state.lock().unwrap();
                let addressbook_locked = self.addressbook.lock().unwrap();
                let mut total_amount = 0;
                let mut _UTXO_inputs:Vec<UTXO_input> = Vec::new();
                let mut _UTXO_outputs:Vec<UTXO_output> = Vec::new();

                // Only generate transactions if there is a known recipient (received from other nodes)
                if addressbook_locked.addresses_vec.len() > 0 {

                    // Select recipient at random
                    let rand_recip:H160 = addressbook_locked.addresses_vec.choose(&mut rand::thread_rng()).unwrap().clone();
        
                    // Gather funds from owned UTXOs that exist in current state
                    for key in state_locked.state.keys(){
                        let owner = state_locked.state[key].1;
                        if owner == addressbook_locked.get_my_address().clone() {
                            total_amount += state_locked.state[key].0;

                            let new_UTXO_input = UTXO_input {
                                prev_tx_hash: key.0, 
                                index: key.1,
                            };
                            _UTXO_inputs.push(new_UTXO_input);
                            if total_amount > 1000 {
                                break;
                            }
                        }
                    }
                    drop(state_locked);

                    if total_amount > 10 {
                        // Allocate funds for recipient
                        let new_UTXO_output1 = UTXO_output {
                            receipient_address: rand_recip,
                            value: 10,
                        };
                        _UTXO_outputs.push(new_UTXO_output1);

                        // Return excess funds to self
                        let new_UTXO_output2 = UTXO_output {
                            receipient_address: addressbook_locked.get_my_address().clone(),
                            value: total_amount-10,
                        }; 
                        _UTXO_outputs.push(new_UTXO_output2);

                        let tx = Transaction {
                            input: _UTXO_inputs, 
                            output: _UTXO_outputs,
                            is_ico: false,
                        };
                        let tx_serial = bincode::serialize(&tx).unwrap();
                        let signature = addressbook_locked.my_identity.sign(&tx_serial);

                        let signed_tx = SignedTransaction {
                            transaction: tx,
                            public_key: addressbook_locked.my_identity.public_key().as_ref().to_vec(), 
                            signature: signature.as_ref().to_vec() 
                        };
            
                        //sanity check
                        // let sc = verify(
                        //     &signed_tx.transaction, 
                        //     &signed_tx.signature,
                        //     &signed_tx.public_key);
                        // debug!("Sanity Check({:?})", sc); 
            
                        let mut mempool_locked = self.mempool.lock().unwrap();
                        if !mempool_locked.contains_double_utxo(&signed_tx) {
                            mempool_locked.insert(&signed_tx);
                        }
                        let signed_tx_hash: H256 = signed_tx.hash();
            
                        drop(addressbook_locked);
                        drop(mempool_locked);

                        // debug!("Autogenerated new transaction({:?}): \n 
                        //     input ({:?})\n
                        //     output ({:?})\n",signed_tx_hash,signed_tx.transaction.input,signed_tx.transaction.output);
                        self.server.broadcast(Message::NewTransactionHashes(vec![signed_tx_hash]));
                    }
                }
            }
            
            thread::sleep(time::Duration::from_millis(1000));
        }
    }
}

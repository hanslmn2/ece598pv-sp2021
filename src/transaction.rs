use serde::{Serialize,Deserialize};
use ring::signature::{self, Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use ring::{digest};
use crate::crypto::hash::{H256, Hashable, H160};


//signed transactions
#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct SignedTransaction {
  pub transaction: Transaction,
  pub signature: Vec<u8>, 
  pub public_key: Vec<u8>,
}

impl Hashable for SignedTransaction {
    fn hash(&self) -> H256 {
        let m = bincode::serialize(&self).unwrap();
        digest::digest(&digest::SHA256, m.as_ref()).into()
    }
}


#[derive(Serialize, Deserialize, Debug, Default,Clone, Eq, PartialEq, Hash)]
pub struct UTXO_input{
  pub prev_tx_hash: H256,
  pub index: u8,  
}

#[derive(Serialize, Deserialize, Debug, Default,Clone)]
pub struct UTXO_output{
  pub receipient_address: H160,
  pub value: u32,
}


#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Transaction {
    pub input: Vec<UTXO_input>,
    pub output: Vec<UTXO_output>,
    pub is_ico: bool,
}
impl Hashable for Transaction {
    fn hash(&self) -> H256 {
        let mut ctx = digest::Context::new(&digest::SHA256);
        ctx.update(bincode::serialize(self).unwrap()[..].as_ref());
        ctx.finish().into()
    }
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let serialized_transaction: Vec<u8> = bincode::serialize(&t).unwrap();
    key.sign(&serialized_transaction[..])
}

pub fn verify(t: &Transaction, signature_bytes: &Vec<u8>, public_key_bytes: &Vec<u8>) -> bool {
    let serialized_transaction: Vec<u8> = bincode::serialize(&t).unwrap();
    let peer_public_key = signature::UnparsedPublicKey::new(&signature::ED25519, public_key_bytes);
    peer_public_key.verify(&serialized_transaction[..], &signature_bytes[..]).is_ok()
}


#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;
    use super::random::generate_random_transaction;
    
    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}



use crate::block::{Block, BlockContent, BlockHeader};
use crate::crypto::hash::{H160, H256, Hashable};
use crate::crypto::merkle::MerkleTree;
use crate::mempool::{Mempool, State};
use crate::transaction::{SignedTransaction};

use std::collections::{HashMap, HashSet};
use std::time::{SystemTime, UNIX_EPOCH, Duration};
use hex_literal::hex;
use log::{info, debug, warn};

#[derive(Debug)]
pub struct Blockchain {
    pub hash_to_block: HashMap<H256, Block>,
    pub hash_to_height: HashMap<H256, usize>,
    pub hash_to_state: HashMap<H256, State>,
    pub blocks_in_longest_chain: HashSet<H256>,
    global_height:u32,
    tip: H256,
    genesis_hash: H256
}

impl Blockchain {
    /// Create a new blockchain, only containing the genesis block
    pub fn new() -> Self {

        let block_content: BlockContent = BlockContent {
            transactions: Vec::new(),
        };

        let block_header = BlockHeader{
            parent_hash: H256::from([0u8;32]),
            nonce: 0u32,
            difficulty: hex!("0F00000000000000000000000000000000000000000000000000000000000000").into(),
            timestamp: Duration::new(0,0),
            merkle_root: MerkleTree::new(&block_content.transactions).root(),
        };

        let genesis_block = Block{
            header: block_header,
            content: block_content,
        };
        let genesis_hash = genesis_block.hash();
        let mut htb = HashMap::new();
        let mut hth = HashMap::new();
        let mut hts = HashMap::new();
        let mut blocks_in_longest_chain = HashSet::new();

        htb.insert(genesis_hash, genesis_block);
        hth.insert(genesis_hash, 0);
        hts.insert(genesis_hash, State::new());
        blocks_in_longest_chain.insert(genesis_hash);

        Blockchain{
            hash_to_block:htb,
            hash_to_height:hth,
            hash_to_state:hts,
            blocks_in_longest_chain: blocks_in_longest_chain,
            global_height:0,
            tip:genesis_hash,
            genesis_hash:genesis_hash
        }
    }

    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &Block, mempool: &mut Mempool, state: &mut State) {
        let new_hash = block.hash();
        let new_height = self.hash_to_height[&(block.header.parent_hash)] + 1;
        self.hash_to_block.insert(new_hash, block.clone());
        self.hash_to_height.insert(new_hash, new_height);
        println!("New block ({:?}) inserted with parent ({:?})", new_hash, block.header.parent_hash);
        
        // State Update
        let mut new_state = self.hash_to_state[&block.header.parent_hash].clone();
        let block_txs = &block.content.transactions;
        for tx in block_txs{
            new_state.update(&tx);
        }
        self.hash_to_state.insert(new_hash.clone(), new_state.clone());

        // New block is the new longest chain
        if new_height > self.hash_to_height[&self.tip] {

            // Update global shared state
            *state = new_state.clone();
            
            if block.header.parent_hash == self.tip {
                // New block is extending existing longest chain, simple case
                self.tip = new_hash;
                self.blocks_in_longest_chain.insert(new_hash);

                // Update mempool
                for signed_tx in &block.content.transactions {
                    mempool.remove(&signed_tx);
                }

            } else {
                println!("Fork occured! Rewinding blocks and recalculating state/mempool.");

                // New block is extending a different chain and replacing portion of longest chain
                let mut ancestor_hash: H256 = block.hash();
                let mut replay_hashes: Vec<H256> = Vec::new();

                // Find common ancestor of old longest chain and new longest chain
                loop {
                    let temp_block = &self.hash_to_block[&ancestor_hash];

                    // Create vector of block hashes to replay when common ancestor is found
                    replay_hashes.push(ancestor_hash.clone());

                    // Common ancestor found when parent is part of longest chain hash set
                    if self.blocks_in_longest_chain.contains(&ancestor_hash) {
                        break;
                    }

                    ancestor_hash = temp_block.header.parent_hash;
                }

                // Rewind mempool and inclusion set
                let mut rewind_hash: H256 = self.tip;
                loop {
                    let temp_block = &self.hash_to_block[&rewind_hash];

                    // Rewind transactions
                    for signed_tx in &temp_block.content.transactions {
                        mempool.rewind(signed_tx);
                    }

                    // Rewind longest chain inclusion set
                    self.blocks_in_longest_chain.remove(&rewind_hash);

                    if rewind_hash == ancestor_hash {
                        break;
                    }

                    rewind_hash = temp_block.header.parent_hash;
                }

                // Replay transactions on mempool from common ancestor to new tip
                for block_hash in replay_hashes {
                    let replay_block = &self.hash_to_block[&block_hash];

                    // Update mempool
                    for signed_tx in &replay_block.content.transactions {
                        mempool.remove(&signed_tx);
                    }

                    // Update inclusion set
                    self.blocks_in_longest_chain.insert(block_hash);
                }

                // Set blockchain tip to the new block and done
                self.tip = new_hash;

            }

            println!("New blockchain tip ({:?}) makes length {:?}", new_hash, new_height);
            // info!("Current state: ({:?})", state);
            // info!("Current mempool: ({:?})", mempool);
        }

        println!("-------------------------");
        println!("Transactions included in the block:");
        for signed_tx in &block.content.transactions {
            println!("{:?} - {:?}", signed_tx.hash(), signed_tx.transaction);
        }

        println!("-------------------------");
        println!("State:");
        for pair in &state.state {
            println!("{:?}", pair);
        }

        // Display info about new block, current UTXO sums, etc
        let mut totals: HashMap<H160, u32> = HashMap::new();
        for key in state.state.keys() {
            let owner = state.state[key].1;
            let amount = state.state[key].0;
            if totals.contains_key(&owner) {
                let temp_total = totals[&owner] + amount;
                totals.remove(&owner);
                totals.insert(owner, temp_total);
            } else {
                totals.insert(owner, amount);
            }
        }
        
        println!("-------------------------");
        for key in totals.keys() {
            println!("Account {:?} has {:?}", key, totals[key]);
        }


        println!("-------------------------");
        // Show TX Mempool
        println!("Mempool transactions:");
        for signed_tx in mempool.txmap.values() {
            println!("{:?} - {:?}", signed_tx.hash(), signed_tx.transaction);
        }

        println!("\n");

    }

    /// Get the last block's hash of the longest chain
    pub fn tip(&self) -> H256 {
        return self.tip;
    }

    /// Get the last block's hash of the longest chain
    #[cfg(any(test, test_utilities))]
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        let mut ret = Vec::new();
        let mut node = self.tip;
        while node != self.genesis_hash {
            ret.push(node);
            let parent_block = &self.hash_to_block[&node];
            node = parent_block.header.parent_hash;
        }
        return ret;

    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::block::test::generate_random_block;
    use crate::crypto::hash::Hashable;

    #[test]
    fn insert_one() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());

    }
}

use crate::network::server::Handle as ServerHandle;
use crate::network::message::Message;
use crate::blockchain::Blockchain;
use crate::block::{Block, BlockHeader, BlockContent};
use crate::crypto::merkle::{MerkleTree};
use crate::crypto::hash::Hashable;
use crate::transaction::{Transaction, SignedTransaction};
use crate::mempool::{Mempool,State};


use log::{debug, info};

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time::{SystemTime, UNIX_EPOCH, Duration};

use std::thread;
use std::sync::{Arc, Mutex};

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    num_mined:u32,
    mempool: Arc<Mutex<Mempool>>,
    state: Arc<Mutex<State>>,
    num_tx:usize,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<Mempool>>,
    state: &Arc<Mutex<State>>,
    num_tx: usize,

) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        num_mined: 0,
        mempool: Arc::clone(mempool),
        state: Arc::clone(state),
        num_tx: num_tx
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }

}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
        }
    }

    fn miner_loop(&mut self) {
        // main mining loop
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // Mining operation
            let mut blockchain_exclusive = self.blockchain.lock().unwrap();
            let mut mempool_locked = self.mempool.lock().unwrap();
            let mut state_locked = self.state.lock().unwrap();

            let mut transactions = Vec::new();
            let mut num_tx_count : usize = 0;
            // Get transactions from mempool and fill vector
            for val in mempool_locked.txmap.values() {
                let tx: SignedTransaction = val.clone();
                // info!("Miner: adding to block: transaction hash: {:?}", tx_hash);
                transactions.push(tx);
                num_tx_count += 1;
                if num_tx_count == self.num_tx{
                    break;
                }
            }
            
            let block_content = BlockContent {
                transactions: transactions,
            };

            let block_header = BlockHeader {
                parent_hash: blockchain_exclusive.tip(),
                nonce: rand::random(),
                difficulty: blockchain_exclusive.hash_to_block[&blockchain_exclusive.tip()].header.difficulty,
                timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
                merkle_root: MerkleTree::new(&block_content.transactions).root(),
            };

            let new_block = Block {
                header: block_header,
                content: block_content,
            };

            // Check if block hash meets difficulty requirements
            let block_hash = new_block.hash();
            if block_hash <= new_block.header.difficulty {
                // info!("New block found ({}) @ time {:?}, num_mined={}", block_hash, new_block.header.timestamp, self.num_mined);

                // Insert mined block into blockchain
                blockchain_exclusive.insert(&new_block, &mut mempool_locked, &mut state_locked);
                self.num_mined = self.num_mined + 1;

                // Broadcast new block to the rest of the network
                self.server.broadcast(Message::NewBlockHashes(Vec::from([block_hash])));
                // debug!("Broadcast: NewBlockHashes({:?})", Vec::from([block_hash]));
            }

            // Unlock mutex for worker threads
            drop(blockchain_exclusive);
            drop(mempool_locked);
            drop(state_locked);

            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}

use super::hash::{Hashable, H256};
use ring::*;
use std::collections::VecDeque;

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    pub hash: H256,
    pub left_max: usize,
    pub right_max: usize,
    pub left: Option<Box<MerkleTree>>,
    pub right: Option<Box<MerkleTree>>
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {

        if data.len() == 0 {
            MerkleTree {
                hash: H256::from([0u8;32]),
                left_max: 0,
                right_max: 0,
                left: None,
                right: None,
            }
        } else {
            // Store nodes in a vector of sorts
            let mut nodes: VecDeque<Option<Box<MerkleTree>>> = VecDeque::new();

            // Generate initial hashes of data and place in chunk_queue
            let mut index: usize = 0;
            for datum in data {
                let new_node = MerkleTree {
                    hash: datum.hash().into(),
                    left_max: index,
                    right_max: index,
                    left: None,
                    right: None,
                };
                let boxed_node = Some(Box::new(new_node));
                // println!("{:?}", boxed_node);
                nodes.push_back(boxed_node);
                index = index + 1;
            }

            // Build up tree till only the root remains
            while nodes.len() > 1 {

                // Duplicate the hash of the last node if row is odd
                if nodes.len() % 2 == 1 {
                    let new_hash: H256 = nodes.back().unwrap().as_ref().unwrap().hash;
                    let new_node = MerkleTree {
                        hash: new_hash,
                        left_max: nodes.back().unwrap().as_ref().unwrap().left_max,
                        right_max: nodes.back().unwrap().as_ref().unwrap().right_max,
                        left: None,
                        right: None,
                    };
                    let boxed_node = Some(Box::new(new_node));
                    // println!("{:?}", boxed_node);
                    nodes.push_back(boxed_node);
                }

                let new_layer_size: usize = (nodes.len()+1)/2;
                for _n in 0..new_layer_size {

                    let left_node: Option<Box<MerkleTree>> = nodes.pop_front().unwrap();
                    let right_node: Option<Box<MerkleTree>> = nodes.pop_front().unwrap();
                    let left_hash: [u8; 32] = left_node.as_ref().unwrap().hash.into();
                    let right_hash: [u8; 32] = right_node.as_ref().unwrap().hash.into();

                    // println!("Left Hash: {:?}", H256::from(left_hash));
                    // println!("Right Hash: {:?}", H256::from(right_hash));

                    // Compute combined hash of two children
                    let mut ctx = digest::Context::new(&digest::SHA256);
                    ctx.update(&left_hash);
                    ctx.update(&right_hash);
                    let new_hash: H256 = ctx.finish().into();
                    // println!("New Hash: {:?}", new_hash);

                    // Create new node from data and link to child nodes
                    let new_node = MerkleTree {
                        hash: new_hash,
                        left_max: left_node.as_ref().unwrap().right_max,
                        right_max: right_node.as_ref().unwrap().right_max,
                        left: left_node,
                        right: right_node,
                    };
                    let boxed_node = Some(Box::new(new_node));
                    // println!("{:?}", boxed_node);
                    nodes.push_back(boxed_node);
                }
            }

            // Return the root node
            // println!("{:?}", nodes.front().unwrap().as_ref().unwrap());
            *nodes.pop_front().unwrap().unwrap()
        }
    }

    pub fn root(&self) -> H256 {
        self.hash
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut node: &MerkleTree = &self;
        let mut proof: Vec<H256> = Vec::new();
        while (!node.left.is_none()) || (!node.right.is_none()) {
            if index <= node.left_max {
                // Data is to the left, push right as sibling hash
                proof.push(node.right.as_ref().unwrap().hash);
                node = node.left.as_ref().unwrap();
            } else {
                // Data is to the right, push left as sibling hash
                proof.push(node.left.as_ref().unwrap().hash);
                node = node.right.as_ref().unwrap();
            }
        }

        // println!("{:?}", proof);
        proof
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut current_hash: H256 = *datum;
    let mut layer_width: usize = leaf_size;
    let mut current_index: usize = index;
    let mut proof_vec: Vec<H256> = Vec::from(proof);

    // println!("{:?}", proof_vec);

    // Move up the Merkle Tree
    while layer_width > 1 {

        let mut ctx = digest::Context::new(&digest::SHA256);
        let sibling_hash: [u8;32] = proof_vec.pop().unwrap().into();
        let other_hash: [u8;32] = current_hash.into();

        // println!("{:?}", sibling_hash);
        // println!("{:?}", other_hash);
        
        if current_index % 2 == 0 {
            // Sibling from proof is right side of hash
            ctx.update(&other_hash);
            ctx.update(&sibling_hash);
        } else {
            // Sibling from proof is left side of hash
            ctx.update(&sibling_hash);
            ctx.update(&other_hash);
        }
        current_hash = ctx.finish().into();

        layer_width = (layer_width+1)/2;
        current_index = (current_index+1)/2;
    }

    *root == current_hash.into()
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}

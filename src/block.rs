use serde::{Serialize, Deserialize};
use std::time::{Duration};
use ring::digest;

use crate::crypto::hash::{H256, Hashable};
use super::transaction::SignedTransaction;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    pub header: BlockHeader,
    pub content: BlockContent
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BlockHeader {
    pub parent_hash: H256,         // a hash pointer to parent block. Please use H256 that we provide.
    pub nonce: u32,                // a random integer that will be used in proof-of-work mining. We suggest to simply use u32.
    pub difficulty: H256,          // the mining difficulty, i.e., the threshold in proof-of-work check. Please use H256 since we provide the comparison function, with which you can simply write if hash <= difficulty. (Proof-of-work check or mining is not required in this part.)
    pub timestamp: Duration,     // the timestamp when this block is generated. This is used to decide the delay of a block in future part of this project.
    pub merkle_root: H256,         // the Merkle root of data, this is the cumulative hash of data in block content
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BlockContent {
    pub transactions: Vec<SignedTransaction>,
}

impl Hashable for Block {
    fn hash(&self) -> H256 {
        let mut ctx = digest::Context::new(&digest::SHA256);
        ctx.update(bincode::serialize(&self).unwrap()[..].as_ref());
        ctx.finish().into()
    }
}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;

    use crate::crypto::hash::H256;
    use crate::transaction::{Transaction, random::generate_random_transaction};
    
    pub fn generate_random_block(parent: &H256) -> Block {

        let mut random_transactions: Vec<Transaction> = Vec::new();
        for _ in 0..10 {
            random_transactions.push(generate_random_transaction());
        } 

        let new_content: BlockContent = BlockContent {
            transactions: random_transactions,
        };
        
        let new_header: BlockHeader = BlockHeader {
            parent_hash: *parent,
            nonce: rand::random(),
            difficulty: H256::from(rand::random::<[u8;32]>()),
            timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
            merkle_root: MerkleTree::new(&new_content.transactions).root(),
        };

        Block {
            header: new_header,
            content: new_content,
        }
    }
}

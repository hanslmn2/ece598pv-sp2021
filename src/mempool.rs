use crate::crypto::hash::{H256, Hashable, H160};
use crate::crypto::key_pair;

use crate::transaction::{SignedTransaction, UTXO_input};
use ring::signature::{Ed25519KeyPair, KeyPair};
use ring::digest;

use std::collections::{HashSet, HashMap};
use log::{debug, warn, info};


pub struct AddressBook {
    pub addresses: HashSet<H160>,
    pub addresses_vec: Vec<H160>,
    pub my_identity: Ed25519KeyPair,
    pub my_address:H160
}
impl AddressBook {
    pub fn new() -> Self {
        let key:Ed25519KeyPair = key_pair::random();
        let pub_key = key.public_key();
        let pub_key_hash: H256 = digest::digest(&digest::SHA256, pub_key.as_ref()).into();
        let address: H160 = pub_key_hash.to_h160().into();

        AddressBook { 
            addresses: HashSet::new(),
            addresses_vec: Vec::new(),
            my_identity: key,
            my_address: address
         }

    }
    pub fn insert_addr(&mut self, new_address: H160) {
        if !self.addresses.contains(&new_address) && new_address != self.my_address {
            self.addresses.insert(new_address);
            self.addresses_vec.push(new_address);
        }

    }
    pub fn get_my_address(&self) -> &H160 {
        &self.my_address
    }

}

#[derive(Debug,Clone)]
pub struct State {
    pub state: HashMap<(H256, u8), (u32, H160)>,
}

impl State {
    pub fn new() -> Self {
        let state = HashMap::new();

        State { state: state }
    }

    pub fn update(&mut self, transaction: &SignedTransaction) {
        let tx = transaction.transaction.clone();
        for txin in tx.input {
            self.state.remove(&(txin.prev_tx_hash, txin.index));
        }

        let mut idx = 0;
        for txout in tx.output {
            let tx_hash = transaction.hash();
            self.state.insert((tx_hash, idx), (txout.value, txout.receipient_address));
            idx += 1;
        }

    }
}

#[derive(Debug)]
pub struct Mempool {
    pub txmap: HashMap<H256, SignedTransaction>, //txs that can be picked up by miner
    pub spent_tx: HashMap<H256, SignedTransaction>, //mined txs that are in blocks somehwere in blockchain
    pub tx_to_block: HashMap<H256, H256>, //TODO mempool should contain unspent txs and those in longest chain only
    pub tx_buffer: HashSet<H256>,  //tried to spend tx not in mempool
    // pub pending_utxos: HashSet<UTXO_input>, // hashes of UTXO inputs that exist in mempool but aren't included in longest chain
    pub utxo_to_tx_hash: HashMap<UTXO_input, H256>, // returns transaction hash given a UTXO input
}

impl Mempool {
    pub fn new() -> Self {
        Mempool { 
            txmap: HashMap::new(),
            spent_tx:HashMap::new(),
            tx_to_block:HashMap::new(),
            tx_buffer: HashSet::new(),
            // pending_utxos: HashSet::new(),
            utxo_to_tx_hash: HashMap::new(),
        }
    }

    pub fn contains_double_utxo(&self, signed_tx: &SignedTransaction) -> bool {
        let mut contains_double_utxo: bool = false;
        for utxo_input in &signed_tx.transaction.input {
            if self.utxo_to_tx_hash.contains_key(&utxo_input) {
                contains_double_utxo = true;
                break;
            }
        }

        return contains_double_utxo;
    }

    pub fn insert(&mut self, transaction: &SignedTransaction) {
        let tx_hash: H256 = transaction.hash();

        if !(self.txmap.contains_key(&tx_hash) || self.spent_tx.contains_key(&tx_hash)){
            self.txmap.insert(tx_hash.clone(), transaction.clone());
            for utxo_input in &transaction.transaction.input {
                self.utxo_to_tx_hash.insert(utxo_input.clone(), tx_hash);
                // self.pending_utxos.insert(utxo_input.clone());
            }
        }
    }

    pub fn size(&self) -> usize {
        self.txmap.len() 
    }

    pub fn get(&mut self, tx_hash: &H256) -> SignedTransaction{
        let tx: SignedTransaction;
        if self.txmap.contains_key(&tx_hash){
            tx = self.txmap[&tx_hash].clone();
        }
        else{
            tx = self.spent_tx[&tx_hash].clone();
        }
        tx
    }

    pub fn contains_hash(&mut self, tx_hash: &H256) -> bool{
        self.txmap.contains_key(&tx_hash) || self.spent_tx.contains_key(&tx_hash)
    }

    pub fn contains_tx(&mut self, transaction: &SignedTransaction) -> bool{
        let tx_hash: H256 = transaction.hash();
        self.txmap.contains_key(&tx_hash) || self.spent_tx.contains_key(&tx_hash)

    }
    
    // Remove a transaction from the mempool and add it to database of spent txs
    pub fn remove(&mut self, transaction: &SignedTransaction) {
        let tx_hash: H256 = transaction.hash();
        self.spent_tx.insert(tx_hash.clone(), transaction.clone());
        for utxo_input in &transaction.transaction.input {
            // Remove any pending transactions that conflict with current transaction
            match self.utxo_to_tx_hash.get(&utxo_input) {
                None => {},
                Some(overlapping_tx_hash) => {
                    self.txmap.remove(&overlapping_tx_hash);
                    self.utxo_to_tx_hash.remove(&utxo_input);
                }
            }
        }
        self.txmap.remove(&tx_hash);
    }

    // Rewind a transaction that has been applied to the longest chain, and return it to the mempool
    pub fn rewind(&mut self, transaction: &SignedTransaction) {
        let tx_hash: H256 = transaction.hash();
        self.spent_tx.remove(&tx_hash);
        self.txmap.insert(tx_hash.clone(), transaction.clone());
        for utxo_input in &transaction.transaction.input {
            self.utxo_to_tx_hash.insert(utxo_input.clone(), tx_hash);
        }
    }
}